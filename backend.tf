terraform {
  backend "s3" {
    bucket = "1cloudyeti-terraform"
    key    = "terraform.tfstate"
    region = "us-east-1"
    profile = "profile1"
  }
}
